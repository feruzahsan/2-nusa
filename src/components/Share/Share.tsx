import React from 'react'
import {
  FacebookOutlined,
  LinkedinOutlined,
  TwitterOutlined,
} from '@ant-design/icons'

const shareToFacebook = () => {
  window.open(
    'https://www.facebook.com/sharer/sharer.php?u=URL_TO_SHARE',
    '_blank',
  )
}

const shareToLinkedIn = () => {
  window.open(
    'https://www.linkedin.com/shareArticle?url=URL_TO_SHARE',
    '_blank',
  )
}

const shareToTwitter = () => {
  window.open(
    'https://twitter.com/intent/tweet?url=URL_TO_SHARE&text=Check%20out%20this%20article!',
    '_blank',
  )
}

function Share() {
  return (
    <div className="w-[65rem] h-[5rem] flex items-center text-[#404258] mt-10">
      <div className="w-[14rem] h-[4rem] ms-3">
        <p>Author</p>
        <div className="flex items-center">
          <img
            src="./static/images/content/Blogdetail/avatar.png"
            alt="avatar"
            width={40}
            className="rounded-md"
          />
          <p className="ms-2">Dahlia Putri</p>
        </div>
      </div>
      <div className="w-[10rem] h-[4rem] ms-auto p">
        <p>Share Article</p>
        <div className="flex items-center">
          <div
            className="rounded-full flex bg-[#A1A5B7] w-[28px] h-[28px] justify-center items-center cursor-pointer"
            onClick={shareToFacebook}
          >
            <FacebookOutlined
              style={{ fontSize: '20px' }}
              onClick={shareToFacebook}
            />
          </div>
          <div
            className="rounded-full mx-2 flex bg-[#A1A5B7] w-[28px] h-[28px] justify-center items-center cursor-pointer"
            onClick={shareToLinkedIn}
          >
            <LinkedinOutlined
              style={{ fontSize: '20px' }}
              onClick={shareToLinkedIn}
            />
          </div>
          <div
            className="rounded-full flex bg-[#A1A5B7] w-[28px] h-[28px] justify-center items-center cursor-pointer"
            onClick={shareToTwitter}
          >
            <TwitterOutlined
              style={{ fontSize: '20px' }}
              onClick={shareToTwitter}
            />
          </div>
        </div>
      </div>
    </div>
  )
}

export default Share
