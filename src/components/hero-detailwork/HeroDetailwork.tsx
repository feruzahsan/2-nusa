import React from 'react'

interface HeroDetailWorkProps {
  Title: string
  Description: string
  Image: any
  Link?: any
  NameLink?: string
}
function HeroDetailwork(props: HeroDetailWorkProps) {
  const {Title,Description,Image,Link,NameLink} = props
  return (
    <div className="text-center mb-[160px]">
      {/* title of the project */}
      <p className="text-[16px] text-[#A1A5B7]">{Description}</p>
      <h1 className="text-[42px] text-[#404258] font-bold mb-12">{Title}</h1>
      {/* image of the project */}
        <div className="flex justify-center">
          <img
            src={Image}
            alt="img-detailOurwork"
            className="text-center w-full rounded-3xl mb-10"
          />
        </div>
        <div className="flex justify-evenly text-left gap-10">
          <span>
            <h3 className="text-[20px] text-[#DF1E36] font-semibold">Client</h3>
            <p className="text-[16px] text-[#404258] font-semibold">Trimegah</p>
          </span>
          <span>
            <h3 className="text-[20px] text-[#DF1E36] font-semibold">Year</h3>
            <p className="text-[16px] text-[#404258] font-semibold">2019</p>
          </span>
          <span>
            <h3 className="text-[20px] text-[#DF1E36] font-semibold">Type</h3>
            <p className="text-[16px] text-[#404258] font-semibold">Website</p>
          </span>
          <span>
            <h3 className="text-[20px] text-[#DF1E36] font-semibold">
              Location
            </h3>
            <p className="text-[16px] text-[#404258] font-semibold">
              Jakarta, Indonesia
            </p>
          </span>
          <span>
            <h3 className="text-[20px] text-[#DF1E36] font-semibold">
              Link
            </h3>
              <a href={Link} className="text-[16px] text-[#404258] font-semibold underline text cursor-pointer">{NameLink}</a>
          </span>
          <span>
            <h3 className="text-[20px] text-[#DF1E36] font-semibold">Client</h3>
            <img src="./static/images/icons/desktop.svg" alt="icon-desktop" />
          </span>
        </div>
    </div>
  )
}

export default HeroDetailwork
